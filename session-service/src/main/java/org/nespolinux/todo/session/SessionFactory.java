package org.nespolinux.todo.session;

import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
class SessionFactory {

    private final static Duration SESSION_DEFAULT_DURATION = Duration.ofMinutes(30);

    Session ofOwner(String username) {
        return Session.builder().owner(username).sessionDuration(SESSION_DEFAULT_DURATION).build();
    }
}
