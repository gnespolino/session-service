package org.nespolinux.todo.session;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.Instant;

@RequiredArgsConstructor(staticName = "of")
@Data
class SessionRepresentation {

    @JsonUnwrapped
    private final Session session;

    public boolean isActive() {
        return SessionUtils.isActive(session);
    }

    public Instant getValidUntil() {
        return SessionUtils.getSessionExpiration(session);
    }

}
