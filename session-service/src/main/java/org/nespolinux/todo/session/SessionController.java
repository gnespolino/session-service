package org.nespolinux.todo.session;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sessions")
class SessionController {
    private final SessionService sessionService;

    @PutMapping("{username}")
    @ResponseStatus(HttpStatus.CREATED)
    String createSession(@PathVariable String username) {
        return sessionService.startSession(username);
    }

    @PatchMapping("{sessionId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void patchSession(@PathVariable String sessionId, @RequestBody Map<String, Object> attributes) {
        sessionService.patch(sessionId, attributes);
    }

    @DeleteMapping("{sessionId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void deleteSession(@PathVariable String sessionId) {
        sessionService.invalidate(sessionId);
    }

    @GetMapping("{sessionId}")
    SessionRepresentation getSession(@PathVariable String sessionId) {
        return SessionRepresentation.of(sessionService.getAndTouchById(sessionId));
    }
}
