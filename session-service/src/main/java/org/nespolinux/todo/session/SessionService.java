package org.nespolinux.todo.session;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.nespolinux.todo.couchbase.AbstractCouchbaseService;
import org.nespolinux.todo.couchbase.CouchbaseDocumentFactory;
import org.nespolinux.todo.couchbase.CouchbaseRepository;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
@Service
@Slf4j
public class SessionService extends AbstractCouchbaseService<Session> {

    @Getter
    private final CouchbaseRepository<Session> couchbaseRepository;
    @Getter
    private final CouchbaseDocumentFactory couchbaseDocumentFactory;
    private final SessionFactory sessionFactory;

    public String startSession(String username) {
        return save(sessionFactory.ofOwner(username)).getId();
    }

    public void patch(String sessionId, Map<String, Object> attributes) {
        rebuildAndSaveIfActive(sessionId, withAttributes(attributes))
                .orElseThrow(() -> new SessionDoesntExistException("Session with id " + sessionId + " doesn't exists or has expired"));
    }

    private Function<Session.SessionBuilder, Session.SessionBuilder> withAttributes(Map<String, Object> attributes) {
        return sessionBuilder -> {
            Optional.ofNullable(attributes)
                    .orElse(Collections.emptyMap())
                    .forEach(sessionBuilder::attribute);
            return sessionBuilder;
        };
    }

    public Session getAndTouchById(String sessionId) {
        return rebuildAndSaveIfActive(sessionId, sessionBuilder -> sessionBuilder.lastAccessedAt(Instant.now()))
                .orElseThrow(() -> new SessionDoesntExistException("Session with id " + sessionId + " doesn't exists or has expired"));
    }

    public void invalidate(String sessionId) {
        rebuildAndSaveIfActive(sessionId, sessionBuilder -> sessionBuilder.sessionDuration(Duration.ofSeconds(0)));
    }

    private Optional<Session> rebuildAndSaveIfActive(String sessionId, Function<Session.SessionBuilder, Session.SessionBuilder> sessionRebuilder) {
        return findById(sessionId, Session.class)
                .filter(SessionUtils::isActive)
                .map(session -> save(sessionRebuilder.apply(session.toBuilder()).build()));
    }

    static class SessionDoesntExistException extends RuntimeException {
        public SessionDoesntExistException(String s) {
            super(s);
        }
    }
}
