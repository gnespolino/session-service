package org.nespolinux.todo.session;

import lombok.experimental.UtilityClass;

import java.time.Instant;

@UtilityClass
class SessionUtils {

    boolean isActive(Session session) {
        return getSessionExpiration(session).isAfter(Instant.now());
    }

    Instant getSessionExpiration(Session session) {
        return session.getLastAccessedAt().plus(session.getSessionDuration());
    }

}
