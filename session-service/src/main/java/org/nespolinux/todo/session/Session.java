package org.nespolinux.todo.session;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import org.nespolinux.todo.couchbase.PersistableCouchbaseDocument;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

@Builder(toBuilder = true, access = AccessLevel.PACKAGE)
@Data
public class Session implements PersistableCouchbaseDocument<Session> {
    private final String id;
    @Builder.Default
    private final Instant createdAt = Instant.now();
    @Builder.Default
    private final Instant lastAccessedAt = Instant.now();
    private final Duration sessionDuration;
    private final String owner;
    @Singular
    private final Map<String, Object> attributes;

    @Override
    public Session rebuildId(String id) {
        return toBuilder().id(id).build();
    }
}
