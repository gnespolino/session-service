package org.nespolinux.todo.couchbase;

public interface PersistableCouchbaseDocument<T> extends IdSupplier, IdRebuilder<T> {
}
