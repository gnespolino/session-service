package org.nespolinux.todo.couchbase;

interface IdRebuilder<T> {
    T rebuildId(String id);
}
