package org.nespolinux.todo.couchbase;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.nespolinux.todo.session.Session;

import java.util.Arrays;

public class CouchbaseDocumentIdUtils {

    public static String buildDocumentId(String id, Class<? extends PersistableCouchbaseDocument> clazz) {
        return DocumentType.prefixForClass(clazz) + "_" + id;
    }


    public static String buildDocumentId(PersistableCouchbaseDocument payload) {
        return buildDocumentId(payload.getId(), payload.getClass());
    }

    @Getter
    @RequiredArgsConstructor
    private enum DocumentType {
        SESSION(Session.class, "SID");

        private final Class<? extends PersistableCouchbaseDocument> type;
        private final String prefix;

        private static String prefixForClass(Class<? extends PersistableCouchbaseDocument> aClass) {
            return Arrays.stream(values())
                    .filter(documentType -> documentType.getType().equals(aClass))
                    .map(DocumentType::getPrefix)
                    .findAny()
                    .orElseThrow(() -> new IllegalArgumentException("Unsupported document type for class: " + aClass));
        }
    }

}
