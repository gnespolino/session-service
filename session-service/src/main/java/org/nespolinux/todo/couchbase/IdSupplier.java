package org.nespolinux.todo.couchbase;

public interface IdSupplier {
    String getId();
}
