package org.nespolinux.todo.couchbase;

import com.couchbase.client.java.repository.annotation.Id;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.couchbase.core.mapping.Document;

@Builder
@Document
@Data
public class CouchbaseDocument<T extends IdSupplier> {
    @Id
    private final String documentId;
    private final T payload;
}
