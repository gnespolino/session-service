package org.nespolinux.todo.couchbase;

import org.springframework.data.couchbase.repository.CouchbasePagingAndSortingRepository;

public interface CouchbaseRepository<T extends IdSupplier> extends CouchbasePagingAndSortingRepository<CouchbaseDocument<T>, String> {
}
