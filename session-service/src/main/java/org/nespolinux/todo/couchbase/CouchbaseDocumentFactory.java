package org.nespolinux.todo.couchbase;

import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

@Service
public class CouchbaseDocumentFactory {

    public <T extends PersistableCouchbaseDocument<T>> CouchbaseDocument<T> build(T payload) {
        payload = Objects.nonNull(payload.getId()) ? payload : payload.rebuildId(UUID.randomUUID().toString());
        return CouchbaseDocument.<T>builder()
                .documentId(CouchbaseDocumentIdUtils.buildDocumentId(payload))
                .payload(payload)
                .build();
    }
}
