package org.nespolinux.todo.couchbase;

import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public abstract class AbstractCouchbaseService<T extends PersistableCouchbaseDocument<T>> {

    public T save(T entity) {
        CouchbaseDocument<T> couchbaseDocument = getCouchbaseDocumentFactory().build(entity);
        return getCouchbaseRepository().save(couchbaseDocument).getPayload();
    }

    public T getById(String id, Class<T> type) {
        return findById(id, type)
                .orElseThrow(() -> new IllegalArgumentException("Unable to locate document by id:" + id));
    }

    public Optional<T> findById(String id, Class<T> type) {
        return getCouchbaseRepository().findById(CouchbaseDocumentIdUtils.buildDocumentId(id, type))
                .map(CouchbaseDocument::getPayload);
    }

    protected abstract CouchbaseRepository<T> getCouchbaseRepository();
    protected abstract CouchbaseDocumentFactory getCouchbaseDocumentFactory();

}
